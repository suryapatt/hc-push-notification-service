package com.healthcompass.pushnotifications.service;

import org.springframework.stereotype.Service;


import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.ApsAlert;
import com.google.firebase.messaging.FcmOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushFcmOptions;
import com.google.firebase.messaging.WebpushNotification;
import com.healthcompass.pushnotifications.request.PushNotificationMessageRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FirebaseMessagingService {

    private final FirebaseMessaging firebaseMessaging;
    //private final FirestoreService firestoreService;

    public FirebaseMessagingService(FirebaseMessaging firebaseMessaging) {
        this.firebaseMessaging = firebaseMessaging;
        //this.firestoreService = firestoreService;
    }


    public String sendNotification(PushNotificationMessageRequest pushNotificationMessageRequest) throws FirebaseMessagingException {
        String returnValue=null;
    	
        log.debug("Constructiong Notification Message object for the reuqest:"+pushNotificationMessageRequest.toString()+"'");
    	//Message message = genericMessage_advanced(pushNotificationMessageRequest);
       //Message message = genericMessage(pushNotificationMessageRequest);
       Message message = genericMessage_advanced(pushNotificationMessageRequest);
        
    	log.info("Sending Notification Message for Firebase Service");
    	log.debug("Message:"+message.toString()+"");
    	//Entity tokeEntity= firestoreService.getToken(token);
    	/*if(pushNotificationMessageRequest.getDeviceType().equals("andriod")) {
    		message = androidMessage(pushNotificationMessageRequest);
    	}
    	if(pushNotificationMessageRequest.getDeviceType().equals("IOS")) {
    		message=apnsMessage(pushNotificationMessageRequest);
    	}
    	if(pushNotificationMessageRequest.getDeviceType().equals("web")) {
    		message=webpushMessage(pushNotificationMessageRequest);
    	}*/
       
         
       
       
        returnValue =  firebaseMessaging.send(message);
        log.info("Received message id from Firebase service ");
    
        log.debug(returnValue);
        log.debug(" for the Message:"+message.toString()+"");
        return returnValue;
        
    }
    
    public Message androidMessage(PushNotificationMessageRequest pushNotificationMessageRequest) {
        // [START android_message]
        Message message = Message.builder()
            .setAndroidConfig(AndroidConfig.builder()
                .setTtl(3600 * 1000) // 1 hour in milliseconds
                .setPriority(AndroidConfig.Priority.NORMAL)
                .setNotification(AndroidNotification.builder()
                    .setTitle(pushNotificationMessageRequest.getTitle())
                    .setBody(pushNotificationMessageRequest.getBody())
                   // .setIcon("stock_ticker_update")
                    //.setColor("#f45342")
                    .build())
                 .build())
            .setToken(pushNotificationMessageRequest.getToken())
            .putAllData(pushNotificationMessageRequest.getExtraData())
            .build();
        // [END android_message]
        return message;
      }

      public Message apnsMessage(PushNotificationMessageRequest pushNotificationMessageRequest) {
        // [START apns_message]
        Message message = Message.builder()
            .setApnsConfig(ApnsConfig.builder()
                .putHeader("apns-priority", "10")
                .setAps(Aps.builder()
                    .setAlert(ApsAlert.builder()
                        .setTitle(pushNotificationMessageRequest.getTitle())
                        .setBody(pushNotificationMessageRequest.getBody())
                        .build())
                    .setBadge(42)
                    .build())
                .build())
            .setToken(pushNotificationMessageRequest.getToken())
            .putAllData(pushNotificationMessageRequest.getExtraData())
            .build();
        // [END apns_message]
        return message;
      }

      public Message webpushMessage(PushNotificationMessageRequest pushNotificationMessageRequest) {
        // [START webpush_message]
        Message message = Message.builder()
            .setWebpushConfig(WebpushConfig.builder()
                .setNotification(new WebpushNotification(
                	pushNotificationMessageRequest.getTitle(),
                	pushNotificationMessageRequest.getBody(),
                    null))
                .setFcmOptions(WebpushFcmOptions.withLink(pushNotificationMessageRequest.getLinkPage()))
                .build())
            .setToken(pushNotificationMessageRequest.getToken())
            .putAllData(pushNotificationMessageRequest.getExtraData())
            .build();
        // [END webpush_message]
        return message;
      }

      public Message androidMessage_advanced(PushNotificationMessageRequest pushNotificationMessageRequest) {
          // [START android_message]
          Message message = Message.builder()
              .setAndroidConfig(AndroidConfig.builder()
                  .setTtl(3600 * 1000) // 1 hour in milliseconds
                  .setCollapseKey("") // collapse key
                  .setPriority(AndroidConfig.Priority.NORMAL)
                  .setNotification(AndroidNotification.builder()
                      .setTitle(pushNotificationMessageRequest.getTitle())
                      .setBody(pushNotificationMessageRequest.getBody())
                     // .setIcon("stock_ticker_update")
                      //.setColor("#f45342")
                      .build())
                   .build())
              .setToken(pushNotificationMessageRequest.getToken())
              .putAllData(pushNotificationMessageRequest.getExtraData())
              .build();
          // [END android_message]
          return message;
        }

        public Message apnsMessage_advanced(PushNotificationMessageRequest pushNotificationMessageRequest) {
          // [START apns_message]
          Message message = Message.builder()
              .setApnsConfig(ApnsConfig.builder()
                  .putHeader("apns-priority", "10")
                  .putHeader("apns-expiration","1604750400")  // expiration in milli seconds
                  .putHeader("apns-collapse-id","")  // collapse key
                  .setAps(Aps.builder()
                      .setAlert(ApsAlert.builder()
                          .setTitle(pushNotificationMessageRequest.getTitle())
                          .setBody(pushNotificationMessageRequest.getBody())
                          .build())
                      .setBadge(42)
                      .build())
                  .build())
              .setToken(pushNotificationMessageRequest.getToken())
              .putAllData(pushNotificationMessageRequest.getExtraData())
              .build();
          // [END apns_message]
          return message;
        }

        public Message webpushMessage_advanced(PushNotificationMessageRequest pushNotificationMessageRequest) {
          // [START webpush_message]
          Message message = Message.builder()
              .setWebpushConfig(WebpushConfig.builder()
                  .setNotification(new WebpushNotification(
                  	pushNotificationMessageRequest.getTitle(),
                  	pushNotificationMessageRequest.getBody(),
                      null)) // null is icon image
                  .setFcmOptions(WebpushFcmOptions.withLink(pushNotificationMessageRequest.getLinkPage()))
                  .build())
                
              .setToken(pushNotificationMessageRequest.getToken())
              .setTopic("") //Collapse key
              .putAllData(pushNotificationMessageRequest.getExtraData())
              .build();
          // [END webpush_message]
          return message;
        }
        
        public Message genericMessage(PushNotificationMessageRequest pushNotificationMessageRequest) {
            // [START android_message]
            Message message = Message.builder()
            		.setAndroidConfig(AndroidConfig.builder()
                            .setTtl(3600 * 1000) // 1 hour in milliseconds
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(AndroidNotification.builder()
                                .setTitle(pushNotificationMessageRequest.getTitle())
                                .setBody(pushNotificationMessageRequest.getBody())
                               // .setIcon("stock_ticker_update")
                                //.setColor("#f45342")
                                .build())
                             .build())

            		.setApnsConfig(ApnsConfig.builder()
                            .putHeader("apns-priority", "10")
                            .setAps(Aps.builder()
                                .setAlert(ApsAlert.builder()
                                    .setTitle(pushNotificationMessageRequest.getTitle())
                                    .setBody(pushNotificationMessageRequest.getBody())
                                    .build())
                                .setBadge(42)
                                .build())
                            .build())
            		.setWebpushConfig(WebpushConfig.builder()
                            .setNotification(new WebpushNotification(
                            	pushNotificationMessageRequest.getTitle(),
                            	pushNotificationMessageRequest.getBody(),
                                null))
                            .setFcmOptions(WebpushFcmOptions.withLink(pushNotificationMessageRequest.getLinkPage()))
                            .build()) 
                .setToken(pushNotificationMessageRequest.getToken())
                .putAllData(pushNotificationMessageRequest.getExtraData())
                .build();
            // [END android_message]
            return message;
          }

        
        public Message genericMessage_advanced(PushNotificationMessageRequest pushNotificationMessageRequest) {
            // [START android_message]
        	
        	Double expiry = pushNotificationMessageRequest.getExpiry();
        	String collapseKey = pushNotificationMessageRequest.getCollapseKey();
        	log.debug("Collapse Key for Push notification message is:"+collapseKey+"");
        	boolean isUseCollapseKey = (collapseKey != null && !collapseKey.isEmpty()) ? true:false;
        	AndroidConfig.Priority messagePriority =  AndroidConfig.Priority.NORMAL; // default normal
        	String priorityAsString ="5" ;// default normal
        	String priorityForWebPush = "normal";
        	Boolean priority = pushNotificationMessageRequest.getPriority();
        	if(priority) {
        		
        		messagePriority = AndroidConfig.Priority.HIGH;
        		priorityAsString="10";
        		priorityForWebPush="high";
        	}else {
        		messagePriority = AndroidConfig.Priority.NORMAL;
        		priorityAsString="5";
        		priorityForWebPush="normal";
        		
        	}
        	
        	log.debug("Setting Push notification message priority to "+messagePriority+" for Android");
    		log.debug("Setting Push notification message priority to "+priorityAsString+" for IOS");
    		log.debug("Setting Push notification message priority to "+priorityForWebPush+" for Web");
        	
        	long messageExpiry = 672*3600*1000; // default 28 days in milliseconds
        	long messageExpiryForWebPush = 672*3600; // default 28 days in seconds
        	if(expiry != null) {
        		messageExpiry = Double.doubleToLongBits(expiry*3600*1000);
        		messageExpiryForWebPush=Double.doubleToLongBits(expiry*3600);
        	}
        	log.debug("Setting Push notification message expiry to "+messageExpiry+" milli seconds for Android");
    		log.debug("Setting Push notification message expiry to "+messageExpiry+" milli seconds for IOS");
    		log.debug("Setting Push notification message expiry to "+messageExpiryForWebPush+" seconds for Web");
            
    		Message message=null;
        	if(isUseCollapseKey) {
        		message = Message.builder()
                		
                		.setAndroidConfig(AndroidConfig.builder()
                                .setTtl(30)  // expiry in milli seconds
                				.setCollapseKey(collapseKey) // collapse key, over writes same message
                                .setPriority(messagePriority) // normal or high
                                .setNotification(AndroidNotification.builder()
                                    .setTitle(pushNotificationMessageRequest.getTitle())
                                    .setBody(pushNotificationMessageRequest.getBody())
                                   // .setIcon("stock_ticker_update")
                                    //.setColor("#f45342")
                                    .build())
                                 .build())
                		      		

                		.setApnsConfig(ApnsConfig.builder()
                                .putHeader("apns-priority", priorityAsString) // 5 - normal, 10 - high
                                //.putHeader("apns-expiration",""+messageExpiry+"")  // expiration in milli seconds
                                .putHeader("apns-expiration","300000")  // expiration in milli seconds
                                .putHeader("apns-collapse-id",collapseKey)  // collapse key
                                .setAps(Aps.builder()
                                    .setAlert(ApsAlert.builder()
                                        .setTitle(pushNotificationMessageRequest.getTitle())
                                        .setBody(pushNotificationMessageRequest.getBody())
                                        .build())
                                    .setBadge(42)
                                    .build())
                                .build())
              		.setWebpushConfig(WebpushConfig.builder()
                                .setNotification(new WebpushNotification(
                                	pushNotificationMessageRequest.getTitle(),
                                	pushNotificationMessageRequest.getBody(),
                                    null)) // null is icon image
                                .setFcmOptions(WebpushFcmOptions.withLink(pushNotificationMessageRequest.getLinkPage()))
                                .putHeader("Urgency", priorityForWebPush) //  normal, high
                               // .putHeader("TTL",""+messageExpiryForWebPush+"")  // expiration in seconds
                                .putHeader("TTL","30")  // expiration in seconds
                                .putHeader("Topic",collapseKey)  // collapse key
                                .build())
                		
                    .setToken(pushNotificationMessageRequest.getToken())
                    .putAllData(pushNotificationMessageRequest.getExtraData())
                    .build();
            	
        	}  else {
        				message = Message.builder()
                		
                		.setAndroidConfig(AndroidConfig.builder()
                                .setTtl(30)  // expiry in milli seconds
                				.setPriority(messagePriority) // normal or high
                                .setNotification(AndroidNotification.builder()
                                    .setTitle(pushNotificationMessageRequest.getTitle())
                                    .setBody(pushNotificationMessageRequest.getBody())
                                   // .setIcon("stock_ticker_update")
                                    //.setColor("#f45342")
                                    .build())
                                 .build())
                		      		

                		.setApnsConfig(ApnsConfig.builder()
                                .putHeader("apns-priority", priorityAsString) // 5 - normal, 10 - high
                                //.putHeader("apns-expiration",""+messageExpiry+"")  // expiration in milli seconds
                                .putHeader("apns-expiration","300000")  // expiration in milli seconds
                                .setAps(Aps.builder()
                                    .setAlert(ApsAlert.builder()
                                        .setTitle(pushNotificationMessageRequest.getTitle())
                                        .setBody(pushNotificationMessageRequest.getBody())
                                        .build())
                                    .setBadge(42)
                                    .build())
                                .build())
              		.setWebpushConfig(WebpushConfig.builder()
                                .setNotification(new WebpushNotification(
                                	pushNotificationMessageRequest.getTitle(),
                                	pushNotificationMessageRequest.getBody(),
                                    null)) // null is icon image
                                .setFcmOptions(WebpushFcmOptions.withLink(pushNotificationMessageRequest.getLinkPage()))
                                .putHeader("Urgency", priorityForWebPush) //  normal, high
                               // .putHeader("TTL",""+messageExpiryForWebPush+"")  // expiration in seconds
                                .putHeader("TTL","30")  // expiration in seconds
                                .build())
                		
                    .setToken(pushNotificationMessageRequest.getToken())
                    .putAllData(pushNotificationMessageRequest.getExtraData())
                    .build();
        	}     	
             
        
            // [END android_message]
            return message;
          }

}
