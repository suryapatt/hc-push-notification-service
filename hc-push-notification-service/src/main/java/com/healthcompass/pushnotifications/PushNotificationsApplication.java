package com.healthcompass.pushnotifications;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.google.auth.oauth2.GoogleCredentials;

import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;

@SpringBootApplication
@EnableConfigurationProperties
public class PushNotificationsApplication {

	@Value("#{'${cors-allowed-origins}'.split(', ')}")
	private List<String> allowedOrigins;
	
	@Value("${service-account-file-name}")
	private String serviceAccountFileName;
	
	@Value("${project-id}")
	private String projectId;
	
	public static void main(String[] args) {
		SpringApplication.run(PushNotificationsApplication.class, args);
	}
	
	@Bean
	FirebaseMessaging firebaseMessaging() throws IOException {
	   GoogleCredentials googleCredentials = GoogleCredentials
			   .fromStream(new ClassPathResource(serviceAccountFileName).getInputStream());
			   
	            //.fromStream(new ClassPathResource("firebase_svc_account.json").getInputStream());
			   //.fromStream(new ClassPathResource("zinc-iterator-310511-firebase-adminsdk-1vfsi-39d80ab5eb.json").getInputStream());
	    FirebaseOptions firebaseOptions = FirebaseOptions
	            .builder()
	            .setCredentials(googleCredentials)
	            .build();
	    FirebaseApp app = FirebaseApp.initializeApp(firebaseOptions, projectId);
	   // FirebaseApp app = FirebaseApp.initializeApp(firebaseOptions, "zinc-iterator-310511");
	 
	    
	    
	    return FirebaseMessaging.getInstance(app);
	}
	
	
	
	
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilterRegistration() {

		// http://stackoverflow.com/questions/31724994/spring-data-rest-and-cors
		CorsConfiguration config = new CorsConfiguration();
		//config.setAllowCredentials(true);
		config.setAllowedOrigins(allowedOrigins);
		config.addAllowedOrigin("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("PATCH");
		config.addAllowedHeader("AUTHORIZATION");
		config.addAllowedHeader("X-Requested-With");
		config.addAllowedHeader("X-Auth-Token");
		config.addAllowedHeader("X-CLIENT-ID");
		config.addAllowedHeader("X-CLIENT_SECRET");
		config.addAllowedHeader("Content-Type");
		config.addAllowedHeader("User-tz");
		
		
		

		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		final FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate(); 
	}

}
