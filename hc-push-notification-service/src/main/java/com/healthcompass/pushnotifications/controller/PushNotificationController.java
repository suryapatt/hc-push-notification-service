package com.healthcompass.pushnotifications.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



import com.google.firebase.messaging.FirebaseMessagingException;
import com.healthcompass.pushnotifications.request.PushNotificationMessageRequest;
import com.healthcompass.pushnotifications.service.FirebaseMessagingService;

import lombok.extern.slf4j.Slf4j;


import com.healthcompass.pushnotifications.model.PushNotificationMessageObject;


@BasePathAwareController
@Slf4j
public class PushNotificationController {
	
    
    
    @Autowired 
    FirebaseMessagingService firebaseService;
    
    @RequestMapping(value = "/pushNotification/send-push-notification", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String sendNotification(@RequestBody PushNotificationMessageRequest pushNotificationMessageRequest) throws FirebaseMessagingException {
    	String returnCode=null;
	    try {
	    log.debug("Received request for Push Notification with request:"+pushNotificationMessageRequest.toString()+"");	
	    returnCode =  firebaseService.sendNotification(pushNotificationMessageRequest);
	    log.info("Completed request for Push Notification with code:"+returnCode+"");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	    return returnCode;
	}
	
	
}
